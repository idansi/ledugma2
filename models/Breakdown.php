<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use app\models\Status;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "breakdown".
 *
 * @property int $id
 * @property string $title
 * @property int $level
 * @property int $status
 */
class Breakdown extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'breakdown';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level', 'status'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'level' => 'Level',
            'status' => 'Status',
        ];
    }

        public function getStatuses(){
            return $this->hasOne(Status::className(),['id'=>'status'] );
        }
}
