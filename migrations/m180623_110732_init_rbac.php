<?php

use yii\db\Migration;

/**
 * Class m180623_110732_init_rbac
 */
class m180623_110732_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $teamleader = $auth->createRole('teamleader');
        $auth->add($teamleader);

        $manager = $auth->createRole('manager');
        $auth->add($manager);

        $member = $auth->createRole('member');
        $auth->add($member);

        $manageUsers = $auth->createPermission('manageUsers');
        $auth->add($manageUsers);

        $createBreakdown = $auth->createPermission('createBreakdown');
        $auth->add($createBreakdown);

        $deleteBreakdown = $auth->createPermission('deleteBreakdown');
        $auth->add($deleteBreakdown);   

        $updateBreakdown = $auth->createPermission('updateBreakdown');
        $auth->add($updateBreakdown);

        $viewOwnUser = $auth->createPermission('viewOwnUser');
        $rule = new \app\rbac\UserRule;
        $auth->add($rule);

        $viewOwnUser->ruleName = $rule->name;                 
        $auth->add($viewOwnUser);    


        $viewUsers = $auth->createPermission('viewUsers');
        $auth->add($viewUsers);

        $changeLevel = $auth->createPermission('changeLevel');
        $auth->add($changeLevel);

        $auth->addChild($admin, $teamleader);
        $auth->addChild($teamleader, $member);
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($admin, $viewUsers);
        $auth->addChild($member, $createBreakdown);
        $auth->addChild($member, $updateBreakdown);
        $auth->addChild($member, $viewOwnUser);
        $auth->addChild($teamleader, $changeLevel);
        $auth->addChild($viewOwnUser, $viewUsers);
        $auth->addChild($member, $deleteBreakdown);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180623_110732_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180623_110732_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
